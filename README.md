# Fulfillment Backend #

## Installation

```bash
# With npm
$ npm install
# With yarn
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```